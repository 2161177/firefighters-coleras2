<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OperationUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'operation_id'
    ];
}
