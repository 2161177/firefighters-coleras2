<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AirVehicle extends Model
{
    protected $fillable = [
        'name',
        'external_id',
        'code',
        'operation_id'
    ];
}
