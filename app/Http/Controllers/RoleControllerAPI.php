<?php

namespace App\Http\Controllers;

use App\Role;
use App\Http\Resources\Role as RoleResource;
use Illuminate\Http\Request;

class RoleControllerAPI extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('page')) {
            return RoleResource::collection(Role::paginate(5));
        } else {
            return RoleResource::collection(Role::all());
        }
    }

    public function show($id)
    {
        return new RoleResource(Role::find($id));
    }

}
