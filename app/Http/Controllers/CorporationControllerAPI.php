<?php

namespace App\Http\Controllers;

use App\Corporation;
use App\Http\Resources\Corporation as CorporationResource;
use Illuminate\Http\Request;

class CorporationControllerAPI extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('page')) {
            return CorporationResource::collection(Corporation::paginate(5));
        } else {
            return CorporationResource::collection(Corporation::all());
        }
    }

    public function show($id)
    {
        return new CorporationResource(Corporation::find($id));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'local' => 'required'
        ]);
        $corporation = new Corporation();
        $corporation->fill($request->all());
        $corporation->save();
        return response()->json(new CorporationResource($corporation), 201);
    }

    public function update(Request $request, $id)
    {
        $corporation = Corporation::findOrFail($id);
        $request->validate([
            'name' => 'required',
            'local' => 'required'
        ]);
        $corporation->update($request->all());
        return new CorporationResource($corporation);
    }
}
