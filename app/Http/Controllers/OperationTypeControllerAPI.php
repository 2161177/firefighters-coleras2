<?php

namespace App\Http\Controllers;

use App\OperationType;
use App\Http\Resources\OperationType as OperationTypeResource;
use Illuminate\Http\Request;

class OperationTypeControllerAPI extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('page')) {
            return OperationTypeResource::collection(OperationType::paginate(5));
        } else {
            return OperationTypeResource::collection(OperationType::all());
        }
    }

    public function show($id)
    {
        return new OperationTypeResource(OperationType::find($id));
    }

}
