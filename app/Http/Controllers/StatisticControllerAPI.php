<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class StatisticControllerAPI extends Controller
{
    public function all()
    {
        $sql = '
        SELECT
          (select count(*) from operations) as operationsCount,
          (select AVG(counts)
           from (select count(*) as counts
                 from individual_firefighters iff
                   join sensor_datas sd on sd.individual_firefighter_id = iff.id
                 group by sd.operation_id) as firefightersPerOperation) as firefightersPerOperation,
          (select AVG(counts)
           from (select count(*) as counts
                 from air_vehicles av
                 group by av.operation_id) as airsPerOperation) as airsPerOperation,
          (select AVG(counts)
           from (select count(*) as counts
                 from car_operations co
                 group by co.operation_id) as carsPerOperation) as carsPerOperation,
          (select AVG(timeOccured)
           from (select (oo.operation_end - oo.created_at) as timeOccured
                 from operations oo) as meanOpenOperation) as meanOpenOperation,
          (select count(*) from operations where operations.combat_phase_id <> 6) as activeOperations,
          (select count(co.id) as counts
           from car_operations co
             join operations oo on oo.id = co.operation_id
           where
             oo.combat_phase_id <> 6) as activeCars,
          (select count(av.id) as counts
           from air_vehicles av
             join operations oo on oo.id = av.operation_id
           where
             oo.combat_phase_id <> 6) as activeAirVehicles
        from dual
        ';

        $result = DB::SELECT($sql);
        return response()->json($result);
    }
}
