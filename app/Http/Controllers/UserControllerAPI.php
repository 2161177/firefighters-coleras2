<?php

namespace App\Http\Controllers;

use App\Http\Resources\User as UserResource;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserControllerAPI extends Controller
{
    public function index(Request $request)
    {
        return UserResource::collection(User::where('role_id', '>=', auth()->user()->role_id)->get());
    }

    public function show($id)
    {
        return new UserResource(User::where('role_id', '>=', auth()->user()->role_id)->where('id', '=', $id)->firstOrFail());
    }

    public function store(Request $request)
    {
        $request->validate([
            'full_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:3',
            'type_id' => 'required|exists:user_types,id',
            'corporation_id' => 'required|exists:corporations,id',
            'role_id' => 'required|exists:roles,id',
            'mec_number' => 'required|integer',
        ]);

        if($request->role_id < auth()->user()->role_id){
            abort(403);
        }

        $user = new User();
        $user->fill($request->all());
        $user->password = Hash::make($user->password);
        $user->save();
        return response()->json(new UserResource($user), 201);
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $request->validate([
            'full_name' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'type_id' => 'required|exists:user_types,id',
            'corporation_id' => 'required|exists:corporations,id',
            'role_id' => 'required|exists:roles,id',
            'mec_number' => 'required|integer',
        ]);
        if($request->role_id < auth()->user()->role_id || $user->role_id < auth()->user()->role_id){
            abort(403);
        }
        if($request->has('password') && $request->password){
            $user->password = Hash::make($request->password);
            $user->save();
        }
        $user->update($request->except(['password']));
        return new UserResource($user);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if($user->role_id < auth()->user()->role_id){
            abort(403);
        }
        $user->delete();
        return response()->json(null, 204);
    }

    public function emailAvailable(Request $request)
    {
        $totalEmail = 1;
        if ($request->has('email') && $request->has('id')) {
            $totalEmail = DB::table('users')->where('email', '=', $request->email)->where('id', '<>', $request->id)->count();
        } else if ($request->has('email')) {
            $totalEmail = DB::table('users')->where('email', '=', $request->email)->count();
        }
        return response()->json($totalEmail == 0);
    }
}
