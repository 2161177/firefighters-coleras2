<?php

namespace App\Http\Controllers;

use App\OperationSeverity;
use App\Http\Resources\OperationSeverity as OperationSeverityResource;
use Illuminate\Http\Request;

class OperationSeverityControllerAPI extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('page')) {
            return OperationSeverityResource::collection(OperationSeverity::paginate(5));
        } else {
            return OperationSeverityResource::collection(OperationSeverity::all());
        }
    }

    public function show($id)
    {
        return new OperationSeverityResource(OperationSeverity::find($id));
    }

}
