<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Resources\Car as CarResource;
use Illuminate\Http\Request;

class CarControllerAPI extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('page')) {
            return CarResource::collection(Car::paginate(5));
        } else {
            return CarResource::collection(Car::all());
        }
    }

    public function show($id)
    {
        return new CarResource(Car::find($id));
    }

    public function store(Request $request)
    {
        $request->validate([
            'license' => 'required',
            'make' => 'required',
            'classification_id' => 'required|exists:car_classifications,id',
            'external_id' => 'required'
        ]);
        $car = new Car();
        $car->fill($request->all());
        $car->save();
        return response()->json(new CarResource($car), 201);
    }

    public function update(Request $request, $id)
    {
        $car = Car::findOrFail($id);
        $request->validate([
            'license' => 'required',
            'make' => 'required',
            'classification_id' => 'required|exists:car_classifications,id',
            'external_id' => 'required'
        ]);
        $car->update($request->all());
        return new CarResource($car);
    }

}
