<?php

namespace App\Http\Controllers;

use App\CombatPhase;
use App\Http\Resources\CombatPhase as CombatPhaseResource;
use Illuminate\Http\Request;

class CombatPhaseControllerAPI extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('page')) {
            return CombatPhaseResource::collection(CombatPhase::paginate(5));
        } else {
            return CombatPhaseResource::collection(CombatPhase::all());
        }
    }

    public function show($id)
    {
        return new CombatPhaseResource(CombatPhase::find($id));
    }

}
