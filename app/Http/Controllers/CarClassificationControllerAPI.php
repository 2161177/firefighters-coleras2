<?php

namespace App\Http\Controllers;

use App\CarClassification;
use App\Http\Resources\CarClassification as CarClassificationResource;
use Illuminate\Http\Request;

class CarClassificationControllerAPI extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('page')) {
            return CarClassificationResource::collection(CarClassification::paginate(5));
        } else {
            return CarClassificationResource::collection(CarClassification::all());
        }
    }

    public function show($id)
    {
        return new CarClassificationResource(CarClassification::find($id));
    }

}
