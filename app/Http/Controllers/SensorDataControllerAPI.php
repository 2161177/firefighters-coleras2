<?php

namespace App\Http\Controllers;

use App\IndividualFirefighter;
use App\OperationType;
use App\Http\Resources\OperationType as OperationTypeResource;
use App\SensorData;
use Illuminate\Http\Request;

class SensorDataControllerAPI extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'temperature' => 'required',
            'distance' => 'required',
            'car_gps' => 'required',
            'heart_rate' => 'required',
            'operation_id' => 'required',
            'individual_firefighter_external_id' => 'required',
            'secret' => 'required'
        ]);

        // substituir posteriormente o secret após a entrega do projeto
        if ($request->secret !== 'ProjetoEI') {
            abort(403);
        }
        $firefighter = IndividualFirefighter::firstOrCreate([
            'external_id' => $request->individual_firefighter_external_id
        ]);

        $sensor = new SensorData();
        $sensor->fill($request->all());
        $sensor->individual_firefighter_id = $firefighter->id;
        $sensor->save();
        return response()->json('Ok', 201);
    }

}
