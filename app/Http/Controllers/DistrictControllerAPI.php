<?php

namespace App\Http\Controllers;

use App\District;
use App\Http\Resources\District as DistrictResource;
use Illuminate\Http\Request;

class DistrictControllerAPI extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('page')) {
            return DistrictResource::collection(District::paginate(5));
        } else {
            return DistrictResource::collection(District::all());
        }
    }

    public function show($id)
    {
        return new DistrictResource(District::find($id));
    }

}
