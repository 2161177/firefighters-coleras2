<?php

namespace App\Http\Controllers;

use App\UserType;
use App\Http\Resources\UserType as UserTypeResource;
use Illuminate\Http\Request;

class UserTypeControllerAPI extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('page')) {
            return UserTypeResource::collection(UserType::paginate(5));
        } else {
            return UserTypeResource::collection(UserType::all());
        }
    }

    public function show($id)
    {
        return new UserTypeResource(UserType::find($id));
    }

}
