<?php

namespace App\Http\Controllers;

use App\AirVehicle;
use App\CarOperation;
use App\IndividualFirefighter;
use App\Operation;
use App\Http\Resources\Operation as OperationResource;
use App\Http\Resources\IndividualFirefighter as IndividualFirefighterResource;
use App\Http\Resources\User as UserResource;
use App\OperationUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OperationControllerAPI extends Controller
{
    public function index(Request $request)
    {
        if (auth()->user()->role_id === 1){
            return OperationResource::collection(Operation::orderBy('id', 'desc')->get());
        }
        else{
            return OperationResource::collection(Operation::orderBy('id', 'desc')->whereHas('operation_user', function (Builder $query) {
                $query->where('user_id', '=', auth()->user()->id);
            })->get());
        }
    }

    public function show($id)
    {
        return new OperationResource(Operation::find($id));
    }

    public function store(Request $request)
    {
        $request->validate([
            'local' => 'required',
            'district_id' => 'required|exists:districts,id',
            'observations' => '',
            'severity_id' => 'required',
            'operation_end' => 'nullable',
            'area_of_impact' => 'nullable',
            'combat_phase_id' => 'required|exists:combat_phases,id',
            'type_id' => 'required|exists:operation_types,id',
            'cars.*' => 'required|integer',
            'air_vehicles.*.name' => 'required',
            'air_vehicles.*.external_id' => 'required',
            'air_vehicles.*.code' => 'required',
        ]);

        DB::beginTransaction();
        $operation = new Operation();
        $operation->fill($request->all());
        $operation->observations = $operation->observations ?? '';
        $operation->save();

        if($request->has('cars')){
            foreach($request->cars as $car){
                CarOperation::firstOrCreate([
                    'operation_id' => $operation->id,
                    'car_id' => $car
                ]);
            }
        }
        if($request->has('air_vehicles')){
            foreach($request->air_vehicles as $air_vehicle){
                AirVehicle::firstOrCreate([
                    'operation_id' => $operation->id,
                    'name' => $air_vehicle['name'],
                    'external_id' => $air_vehicle['external_id'],
                    'code' => $air_vehicle['code']
                ]);
            }
        }


        DB::commit();

        return response()->json(new OperationResource($operation), 201);
    }

    public function update(Request $request, $id)
    {
        if(auth()->user()->role_id !== 1){
            Operation::where('id', '=', $id)->whereHas('operation_user', function (Builder $query) {
                $query->where('user_id', '=', auth()->user()->id);
            })->firstOrFail();
        }

        $operation = Operation::findOrFail($id);
        $request->validate([
            'local' => 'required',
            'district_id' => 'required|exists:districts,id',
            'observations' => '',
            'severity_id' => 'required',
            'operation_end' => 'nullable',
            'area_of_impact' => 'nullable',
            'combat_phase_id' => 'required|exists:combat_phases,id',
            'type_id' => 'required|exists:operation_types,id',
            'cars.*' => 'required|integer',
            'air_vehicles.*.name' => 'required',
            'air_vehicles.*.external_id' => 'required',
            'air_vehicles.*.code' => 'required',
        ]);
        DB::beginTransaction();
        $request->request->add(['observations' => $request->observations ?: '']);
        $operation->update($request->all());
        $operation->car_operations()->delete();
        $operation->air_vehicles()->delete();
        if($request->has('cars')){
            foreach($request->cars as $car){
                CarOperation::firstOrCreate([
                    'operation_id' => $operation->id,
                    'car_id' => $car
                ]);
            }
        }
        if($request->has('air_vehicles')){
            foreach($request->air_vehicles as $air_vehicle){
                AirVehicle::firstOrCreate([
                    'operation_id' => $operation->id,
                    'name' => $air_vehicle['name'],
                    'external_id' => $air_vehicle['external_id'],
                    'code' => $air_vehicle['code']
                ]);
            }
        }


        DB::commit();
        return new OperationResource($operation);
    }

    public function addUser(Request $request, $id)
    {
        if(auth()->user()->role_id !== 1){
            Operation::where('id', '=', $id)->whereHas('operation_user', function (Builder $query) {
                $query->where('user_id', '=', auth()->user()->id);
            })->firstOrFail();
        }
        $operation = Operation::findOrFail($id);
        $request->validate([
            'user_id' => 'required|exists:users,id'
        ]);
        $operationUser = OperationUser::firstOrCreate([
            'operation_id' => $operation->id,
            'user_id' => $request->user_id
        ]);

        return new OperationResource($operationUser);
    }
    public function removeUser(Request $request, $id, $user_id)
    {
        if(auth()->user()->role_id !== 1){
            Operation::where('id', '=', $id)->whereHas('operation_user', function (Builder $query) {
                $query->where('user_id', '=', auth()->user()->id);
            })->firstOrFail();
        }
        $operation = Operation::findOrFail($id);

        $operationUser = OperationUser::where([
            'operation_id' => $operation->id,
            'user_id' => $user_id
        ])->firstOrFail();
        $operationUser->delete();

    }

    public function getFirefighters(Operation $operation){
        $firefighters = $operation->firefighters()
            ->orderBy('sensor_datas.id', 'desc')
            ->select(
                'individual_firefighters.id',
                'individual_firefighters.external_id',
                'sensor_datas.temperature',
                'sensor_datas.distance',
                'sensor_datas.car_gps',
                'sensor_datas.heart_rate'
            )
            ->get();
        return IndividualFirefighterResource::collection($firefighters);
    }
    public function getUsers(Operation $operation){
        $users = $operation->users()->get();
        return UserResource::collection($users);
    }
}
