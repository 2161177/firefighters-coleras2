<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class IndividualFirefighter extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'external_id' => $this->external_id,
            'temperature' => $this->temperature,
            'distance' => $this->distance,
            'car_gps' => $this->car_gps,
            'heart_rate' => $this->heart_rate,
        ];
    }
}
