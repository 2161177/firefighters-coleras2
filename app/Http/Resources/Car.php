<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Car extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'license' => $this->license,
            'make' => $this->make,
            'classification_id' => $this->classification_id,
            'external_id' => $this->external_id,

            'classification' => $this->classification,
        ];
    }
}
