<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Operation extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'local' => $this->local,
            'district_id' => $this->district_id,
            'observations' => $this->observations,
            'severity_id' => $this->severity_id,
            'operation_end' => $this->operation_end,
            'area_of_impact' => $this->area_of_impact,
            'combat_phase_id' => $this->combat_phase_id,
            'type_id' => $this->type_id,



            'district' => $this->district,
            'severity' => $this->severity,
            'combat_phase' => $this->combat_phase,
            'type' => $this->type,


            'cars' => $this->cars,
            'air_vehicles' => $this->air_vehicles,
        ];
    }
}
