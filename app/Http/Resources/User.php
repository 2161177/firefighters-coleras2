<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class User extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'full_name' => $this->full_name,
            'mec_number' => $this->mec_number,
            'type_id' => $this->type_id,
            'corporation_id' => $this->corporation_id,
            'role_id' => $this->role_id,
            'email' => $this->email,

            'type' => $this->type,
            'corporation' => $this->corporation,
            'role' => $this->role
        ];
    }
}
