<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'license',
        'make',
        'classification_id',
        'external_id'
    ];
    public function classification()
    {
        return $this->belongsTo(CarClassification::class);
    }
}
