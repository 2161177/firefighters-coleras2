<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SensorData extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'temperature',
        'distance',
        'car_gps',
        'heart_rate',
        'operation_id',
        'individual_firefighter_id',
    ];
}
