<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'local',
        'district_id',
        'observations',
        'severity_id',
        'operation_end',
        'area_of_impact',
        'combat_phase_id',
        'type_id'
    ];

    public function district()
    {
        return $this->belongsTo(District::class);
    }
    public function severity()
    {
        return $this->belongsTo(OperationSeverity::class);
    }
    public function combat_phase()
    {
        return $this->belongsTo(CombatPhase::class);
    }
    public function cars()
    {
        return $this->belongsToMany(Car::class, 'car_operations');
    }
    public function car_operations()
    {
        return $this->hasMany(CarOperation::class);
    }
    public function air_vehicles()
    {
        return $this->hasMany(AirVehicle::class);
    }
    public function type()
    {
        return $this->belongsTo(OperationType::class);
    }
    public function users()
    {
        return $this->belongsToMany('App\User', 'operation_users');
    }
    public function operation_user()
    {
        return $this->hasMany(OperationUser::class);
    }
    public function firefighters()
    {
        return $this->hasManyThrough(
            'App\IndividualFirefighter',
            'App\SensorData',
            'operation_id',
            'id',
            'id',
            'individual_firefighter_id'
        );
    }
}
