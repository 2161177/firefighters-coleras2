<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndividualFirefighter extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'external_id'
    ];
}
