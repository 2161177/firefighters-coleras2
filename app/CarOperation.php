<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarOperation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'car_id',
        'operation_id'
    ];
}
