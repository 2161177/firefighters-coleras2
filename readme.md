##Dados de utilizadores:
- 1
    -    **email**: nacional@teste.com
    -    **password**: secret

(outros utilizadores com diferentes níveis de permissão)

- 2
    -    **email**: local@teste.com
    -    **password**: secret

- 3
    -    **email**: operador@teste.com
    -    **password**: secret

## Como correr o projeto
composer update
npm install
npm run watch

(se for necessário adicionar informação à base de dados)
php artisan migrations:fresh --seed
php artisan passport:install
