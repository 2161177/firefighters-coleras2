/*jshint esversion: 6 */

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import router from './router'
// import Vuetify from 'vuetify'
// import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'
import vuetify from './plugins/vuetify' // path to vuetify export
import * as VueGoogleMaps from 'vue2-google-maps'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import VueApexCharts from 'vue-apexcharts'




require('./bootstrap');

window.Vue = require('vue');

Vue.component('app', require('./views/App.vue'));
Vue.component('apexchart', VueApexCharts)

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyDFvcFNOWC3nCKQa65LIX0KrusmEDWnzZI',
        libraries: 'places', // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)

        //// If you want to set the version, you can do so:
        // v: '3.26',
    },

    //// If you intend to programmatically custom event listener code
    //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
    //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
    //// you might need to turn this on.
    // autobindAllEvents: false,

    //// If you want to manually install components, e.g.
    //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
    //// Vue.component('GmapMarker', GmapMarker)
    //// then disable the following:
    // installComponents: true,
})

Vue.use(VueSweetalert2);
Vue.router = router;

// Add a request interceptor
axios.interceptors.request.use(function (config) {
    const token = localStorage.getItem('token');
    config.headers.Authorization = 'Bearer ' + token;

    return config;
});


// Vue.use(Vuetify);

console.log(1)
const app = new Vue({
    el: '#app',
    vuetify,
    router: router,
    data: {
        user: {}
    },
    methods: {
    },
    mounted() {
        axios.get('/api/user')
            .then(response => {
                this.user = response.data.data;
            })
            .catch(response => {
                this.$router.push('/login')
            });
    }
});
