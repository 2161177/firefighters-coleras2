import Vue from 'vue'
import Router from 'vue-router'
import UsersListComponent from "../views/users/UsersListComponent.vue";
import UsersEditComponent from "../views/users/UsersEditComponent.vue";
import UsersNewComponent from "../views/users/UsersNewComponent.vue";
import CarsListComponent from "../views/cars/CarsListComponent.vue";
import CarsEditComponent from "../views/cars/CarsEditComponent.vue";
import CarsNewComponent from "../views/cars/CarsNewComponent.vue";
import CorporationsListComponent from "../views/corporations/CorporationsListComponent.vue";
import CorporationsEditComponent from "../views/corporations/CorporationsEditComponent.vue";
import CorporationsNewComponent from "../views/corporations/CorporationsNewComponent.vue";
import OperationsListComponent from "../views/operations/OperationsListComponent.vue";
import OperationsEditComponent from "../views/operations/OperationsEditComponent.vue";
import OperationsNewComponent from "../views/operations/OperationsNewComponent.vue";
import OperationsOverviewComponent from "../views/operations/OperationsOverviewComponent.vue";
import StatisticsViewComponent from "../views/statistics/StatisticsViewComponent.vue";
import LoginViewComponent from "../views/account/LoginViewComponent.vue";

Vue.use(Router);

var authOnly = (to, from, next) => {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    console.log(this.a.app.$root.$data)
    if (!this.a.app.$root.$data || this.a.app.$root.$data.user === {} || !this.a.app.$root.$data.user.full_name) {
        next({
            path: '/login',
            query: {redirect: to.fullPath}
        })
    } else {
        next()
    }
}
export default new Router({
    routes: [
        {
            path: '/',
            name: 'operations-list',
            component: OperationsListComponent,
            beforeEnter: authOnly
        },
        {
            path: '/users',
            name: 'users-list',
            component: UsersListComponent,
            beforeEnter: authOnly
        },
        {
            path: '/users/edit/:id',
            name: 'users-edit',
            component: UsersEditComponent,
            beforeEnter: authOnly
        },
        {
            path: '/users/new',
            name: 'users-new',
            component: UsersNewComponent,
            beforeEnter: authOnly
        },
        {
            path: '/cars',
            name: 'cars-list',
            component: CarsListComponent,
            beforeEnter: authOnly
        },
        {
            path: '/cars/edit/:id',
            name: 'cars-edit',
            component: CarsEditComponent,
            beforeEnter: authOnly
        },
        {
            path: '/cars/new',
            name: 'cars-new',
            component: CarsNewComponent,
            beforeEnter: authOnly
        },
        {
            path: '/corporations',
            name: 'corporations-list',
            component: CorporationsListComponent,
            beforeEnter: authOnly
        },
        {
            path: '/corporations/edit/:id',
            name: 'corporations-edit',
            component: CorporationsEditComponent,
            beforeEnter: authOnly
        },
        {
            path: '/corporations/new',
            name: 'corporations-new',
            component: CorporationsNewComponent,
            beforeEnter: authOnly
        },
        {
            path: '/operations',
            name: 'operations-list',
            component: OperationsListComponent,
            beforeEnter: authOnly
        },
        {
            path: '/operations/edit/:id',
            name: 'operations-edit',
            component: OperationsEditComponent,
            beforeEnter: authOnly
        },
        {
            path: '/operations/new',
            name: 'operations-new',
            component: OperationsNewComponent,
            beforeEnter: authOnly
        },
        {
            path: '/operations/view/:id',
            name: 'operations-overview',
            component: OperationsOverviewComponent,
            beforeEnter: authOnly
        },
        {
            path: '/statistics',
            name: 'statistics-list',
            component: StatisticsViewComponent,
            meta: { requiresAuth: true },
            beforeEnter: authOnly
        },
        {
            path: '/login',
            name: 'login',
            component: LoginViewComponent,
            beforeEnter: (to, from, next) => {
                setTimeout(() => {

                    // this route requires auth, check if logged in
                    // if not, redirect to login page.
                    console.log(this.a.app.$root.$data)
                    if (!this.a.app.$root.$data || this.a.app.$root.$data.user === {} || !this.a.app.$root.$data.user.full_name) {
                        next()
                    } else {
                        next({
                            path: to.query.redirect || '/operations'
                        })
                    }
                }, 800);
            }
        },
    ]
})
