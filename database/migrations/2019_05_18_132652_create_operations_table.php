<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('local');
            $table->integer('district_id')->unsigned();
            $table->foreign('district_id')->references('id')->on('districts');
            $table->string('observations');
            $table->integer('severity_id')->unsigned();
            $table->foreign('severity_id')->references('id')->on('operation_severities');
            $table->timestamp('operation_end')->nullable();
            $table->integer('area_of_impact')->nullable();
            $table->integer('combat_phase_id')->unsigned();
            $table->foreign('combat_phase_id')->references('id')->on('combat_phases');
            $table->integer('type_id')->unsigned();
            $table->foreign('type_id')->references('id')->on('operation_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operations');
    }
}
