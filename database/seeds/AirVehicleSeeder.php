<?php

use Illuminate\Database\Seeder;

class AirVehicleSeeder extends Seeder
{
    public $numberOfAirVehicles= 30;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_PT');

        $operations = DB::table('operations')->pluck('id')->toArray();

        for ($i = 0; $i < $this->numberOfAirVehicles; ++$i) {
            DB::table('air_vehicles')->insert(
                $this->fakeAirVehicle(
                    $faker,
                    $faker->randomElement($operations)
                ));
        }
    }

        private function fakeAirVehicle(Faker\Generator $faker, $operationId)
    {
        $createdAt = Carbon\Carbon::now()->subDays(30);
        $updatedAt = $faker->dateTimeBetween($createdAt);
        return [
            'name' =>$faker->text,
            'external_id' =>uniqid(),
            'code' =>$faker->text,
            'operation_id' =>$operationId,
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
        ];
    }
}
