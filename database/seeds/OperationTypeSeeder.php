<?php

use Illuminate\Database\Seeder;

class OperationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Incêndio',
            'Salvamento',
            'Estruturas'
        ];
        $createdAt = Carbon\Carbon::now()->subMonths(2);
        foreach ($names as $name) {
            DB::table('operation_types')->insert([
                'type' => $name,
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ]);
        }
    }
}
