<?php

use Illuminate\Database\Seeder;

class IndividualFireFighterSeeder extends Seeder
{
    public $numberOfIndividualFireFighters = 150;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_PT');

        for ($i = 0; $i < $this->numberOfIndividualFireFighters; ++$i) {
            DB::table('individual_firefighters')->insert(
                $this->fakeIndividualFirefighters(
                    $faker
                ));
        }
    }

    private function fakeIndividualFirefighters(Faker\Generator $faker)
    {
        $createdAt = Carbon\Carbon::now()->subDays(30);
        $updatedAt = $faker->dateTimeBetween($createdAt);
        return [
            'external_id' => uniqid(),
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
        ];
    }
}
