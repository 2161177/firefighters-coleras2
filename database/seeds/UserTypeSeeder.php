<?php

use Illuminate\Database\Seeder;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userTypes = [
            'Bombeiros',
            'Proteção Civil'
        ];
        $createdAt = Carbon\Carbon::now()->subMonths(2);
        foreach ($userTypes as $userType) {
            DB::table('user_types')->insert([
                'type' => $userType,
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ]);
        }
    }
}
