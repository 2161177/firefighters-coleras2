<?php

use Illuminate\Database\Seeder;

class CorporationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $createdAt = Carbon\Carbon::now()->subMonths(2);
        $corporations = [
            [
                'name' => 'Bombeiros Voluntários da Maceira',
                'local' => 'Maceira',
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ],
            [
                'name' => 'Bombeiros Voluntários de Leiria',
                'local' => 'Leiria',
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ],
            [
                'name' => 'Bombeiros Voluntários de Coimbra',
                'local' => 'Coimbra',
                'created_at' => $createdAt,
                'updated_at' => $createdAt,

            ],
            [
                'name' => 'Bombeiros Voluntários de Lisboa',
                'local' => 'Lisboa',
                'created_at' => $createdAt,
                'updated_at' => $createdAt,

            ],
            [
                'name' => 'Bombeiros Voluntários de Oeiras',
                'local' => 'Oeiras',
                'created_at' => $createdAt,
                'updated_at' => $createdAt,

            ],
            [
                'name' => 'Proteção Civil',
                'local' => 'Nacional',
                'created_at' => $createdAt,
                'updated_at' => $createdAt,

            ],
        ];

        DB::table('corporations')->insert($corporations);
    }
}
