<?php

use Illuminate\Database\Seeder;

class SensorDataSeeder extends Seeder
{
    public $numberOfSensorData = 300;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_PT');

        $operations = DB::table('operations')->pluck('id')->toArray();
        $individualFirefighters = DB::table('individual_firefighters')->pluck('id')->toArray();

        for ($i = 0; $i < $this->numberOfSensorData; ++$i) {
            DB::table('sensor_datas')->insert(
                $this->fakeSensorData(
                    $faker,
                    $faker->randomElement($operations),
                    $faker->randomElement($individualFirefighters)
                ));
        }
    }

    private function fakeSensorData(Faker\Generator $faker, $operationId, $individualFirefighterId)
    {
        $createdAt = Carbon\Carbon::now()->subDays(30);
        $updatedAt = $faker->dateTimeBetween($createdAt);
        return [
            'temperature' => $faker->numberBetween(10, 90),
            'distance' => $faker->numberBetween(1,300),
            'car_gps' => $faker->latitude . ' ' . $faker->longitude,
            'heart_rate' => $faker->numberBetween(30,200),
            'operation_id' => $operationId,
            'individual_firefighter_id' => $individualFirefighterId,
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
        ];
    }
}
