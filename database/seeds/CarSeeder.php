<?php

use Illuminate\Database\Seeder;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $createdAt = Carbon\Carbon::now()->subMonths(2);
        $cars = [
            [
                'license' => '6656 CXY',
                'make' => 'Volvo',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ],
            [
                'license' => '6626 CEA',
                'make' => 'Opel',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ],
            [
                'license' => '3426 QCC',
                'make' => 'Man',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,

            ],
            [
                'license' => '6626 CEA',
                'make' => 'Opel',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ],
            [
                'license' => '3426 QCC',
                'make' => 'Man',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,

            ],
            [
                'license' => '6626 CEA',
                'make' => 'Opel',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ],
            [
                'license' => '3426 QCC',
                'make' => 'Man',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,

            ],
            [
                'license' => '6626 CEA',
                'make' => 'Opel',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ],
            [
                'license' => '3426 QCC',
                'make' => 'Man',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,

            ],
            [
                'license' => '6626 CEA',
                'make' => 'Opel',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ],
            [
                'license' => '3426 QCC',
                'make' => 'Man',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,

            ],
            [
                'license' => '6626 CEA',
                'make' => 'Opel',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ],
            [
                'license' => '3426 QCC',
                'make' => 'Man',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,

            ],
            [
                'license' => '6626 CEA',
                'make' => 'Opel',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ],
            [
                'license' => '3426 QCC',
                'make' => 'Man',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,

            ],
            [
                'license' => '6626 CEA',
                'make' => 'Opel',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ],
            [
                'license' => '3426 QCC',
                'make' => 'Man',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,

            ],
            [
                'license' => '6626 CEA',
                'make' => 'Opel',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ],
            [
                'license' => '3426 QCC',
                'make' => 'Man',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,

            ],
            [
                'license' => '6626 CEA',
                'make' => 'Opel',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ],
            [
                'license' => '3426 QCC',
                'make' => 'Man',
                'classification_id' => \App\CarClassification::inRandomOrder()->first()->id,
                'external_id' => uniqid(),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,

            ]
        ];

        DB::table('cars')->insert($cars);
    }
}
