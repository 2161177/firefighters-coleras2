<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DistrictSeeder::class);
        $this->call(UserTypeSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(CarClassificationSeeder::class);
        $this->call(CombatPhaseSeeder::class);
        $this->call(OperationSeveritySeeder::class);
        $this->call(OperationTypeSeeder::class);

        $this->call(CorporationSeeder::class);
        $this->call(CarSeeder::class);
        $this->call(UserSeeder::class);

        $this->call(OperationSeeder::class);

        $this->call(AirVehicleSeeder::class);
        $this->call(CarOperationSeeder::class);
        $this->call(OperationUserSeeder::class);

        $this->call(IndividualFireFighterSeeder::class);
        $this->call(SensorDataSeeder::class);
    }
}
