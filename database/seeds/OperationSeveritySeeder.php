<?php

use Illuminate\Database\Seeder;

class OperationSeveritySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Muito baixa',
            'Baixa',
            'Média',
            'Alta',
            'Muito Alta'
        ];
        $createdAt = Carbon\Carbon::now()->subMonths(2);
        foreach ($names as $name) {
            DB::table('operation_severities')->insert([
                'severity' => $name,
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ]);
        }
    }
}
