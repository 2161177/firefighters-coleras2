<?php

use Illuminate\Database\Seeder;

class OperationSeeder extends Seeder
{
    public $numberOfOperations = 30;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_PT');

        $districts = DB::table('districts')->pluck('id')->toArray();
        $severities = DB::table('operation_severities')->pluck('id')->toArray();
        $combatPhases = DB::table('combat_phases')->pluck('id')->toArray();
        $types = DB::table('operation_types')->pluck('id')->toArray();

        for ($i = 0; $i < $this->numberOfOperations; ++$i) {
            DB::table('operations')->insert(
                $this->fakeOperation(
                    $faker,
                    $faker->randomElement($districts),
                    $faker->randomElement($severities),
                    $faker->randomElement($combatPhases),
                    $faker->randomElement($types)
                ));
        }
    }

        private function fakeOperation(Faker\Generator $faker, $districtId, $severityId, $combatPhaseId, $typeId)
    {
        $createdAt = Carbon\Carbon::now()->subDays(30);
        $updatedAt = $faker->dateTimeBetween($createdAt);
        return [
            'local' =>$faker->city,
            'district_id' =>$districtId,
            'observations' => $faker->text,
            'severity_id' => $severityId,
            'operation_end' => $updatedAt,
            'area_of_impact' => $faker->numberBetween(0, 9999),
            'combat_phase_id' =>$combatPhaseId,
            'type_id' =>$typeId,
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
        ];
    }
}
