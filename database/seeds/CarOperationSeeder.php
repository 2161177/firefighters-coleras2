<?php

use Illuminate\Database\Seeder;

class CarOperationSeeder extends Seeder
{
    public $numberOfCarOperations = 30;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_PT');

        $cars = DB::table('cars')->pluck('id')->toArray();
        $operations = DB::table('operations')->pluck('id')->toArray();

        for ($i = 0; $i < $this->numberOfCarOperations; ++$i) {
            DB::table('car_operations')->insert(
                $this->fakeCarOperation(
                    $faker,
                    $faker->randomElement($cars),
                    $faker->randomElement($operations)
                ));
        }
    }

    private function fakeCarOperation(Faker\Generator $faker, $carId, $operationId)
    {
        $createdAt = Carbon\Carbon::now()->subDays(30);
        $updatedAt = $faker->dateTimeBetween($createdAt);
        return [
            'car_id' =>$carId,
            'operation_id' =>$operationId,
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
        ];
    }
}
