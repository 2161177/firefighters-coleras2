<?php

use Illuminate\Database\Seeder;

class CombatPhaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $combatPhases = [
            'Primeiro alerta',
            'Combate',
            'Circunscrição',
            'Rescaldo',
            'Observação',
            'Concluído'
        ];
        $createdAt = Carbon\Carbon::now()->subMonths(2);
        foreach ($combatPhases as $combatPhase) {
            DB::table('combat_phases')->insert([
                'phase' => $combatPhase,
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ]);
        }
    }
}
