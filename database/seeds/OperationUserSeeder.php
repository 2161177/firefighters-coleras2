<?php

use Illuminate\Database\Seeder;

class OperationUserSeeder extends Seeder
{
    public $numberOfOperationUsers = 150;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_PT');

        $users = DB::table('users')->pluck('id')->toArray();
        $operations = DB::table('operations')->pluck('id')->toArray();

        for ($i = 0; $i < $this->numberOfOperationUsers; ++$i) {
            DB::table('operation_users')->insert(
                $this->fakeOperationUser(
                    $faker,
                    $faker->randomElement($users),
                    $faker->randomElement($operations)
                ));
        }
    }

    private function fakeOperationUser(Faker\Generator $faker, $userId, $operationId)
    {
        $createdAt = Carbon\Carbon::now()->subDays(30);
        $updatedAt = $faker->dateTimeBetween($createdAt);
        return [
            'user_id' => $userId,
            'operation_id' => $operationId,
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
        ];
    }
}
