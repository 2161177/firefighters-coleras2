<?php

use Illuminate\Database\Seeder;

class CarClassificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'VTT (Veículo Tanque Táctico)',
            'VCI (Veículo combate de incêndios)',
            'AB (Ambulância)',
            'VT (Veículos de transporte)',
            'VC (Veículo de comando)',
            'VAPA (Veículo de apoio alimentar)',
            'VTGC (Veículo tanque grande capacidade)'
        ];
        $createdAt = Carbon\Carbon::now()->subMonths(2);
        foreach ($names as $name) {
            DB::table('car_classifications')->insert([
                'name' => $name,
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ]);
        }
    }
}
