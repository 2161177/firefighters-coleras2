<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Comando Nacional',
            'Comando Local',
            'Operador',
            'Bombeiro'
        ];
        $createdAt = Carbon\Carbon::now()->subMonths(2);
        foreach ($names as $name) {
            DB::table('roles')->insert([
                'role' => $name,
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ]);
        }
    }
}
