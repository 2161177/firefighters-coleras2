<?php

use Illuminate\Database\Seeder;

class DistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Aveiro',
            'Beja',
            'Braga',
            'Bragança',
            'Castelo Branco',
            'Coimbra',
            'Évora',
            'Faro',
            'Guarda',
            'Leiria',
            'Lisboa',
            'Portalegre',
            'Porto',
            'Santarém',
            'Setúbal',
            'Viana do Castelo',
            'Vila Real',
            'Viseu',
        ];
        $createdAt = Carbon\Carbon::now()->subMonths(2);
        foreach ($names as $name) {
            DB::table('districts')->insert([
                'district' => $name,
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ]);
        }
    }
}
