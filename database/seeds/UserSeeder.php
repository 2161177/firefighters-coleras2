<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public $numberOfUsers = 30;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_PT');

        $userTypes = DB::table('user_types')->pluck('id')->toArray();
        $corporations = DB::table('corporations')->pluck('id')->toArray();
        $roles = DB::table('roles')->pluck('id')->toArray();

        for ($i = 0; $i < $this->numberOfUsers; ++$i) {
            DB::table('users')->insert(
                $this->fakeUser(
                    $faker,
                    $faker->randomElement($userTypes),
                    $faker->randomElement($corporations),
                    $faker->randomElement($roles)
                ));
        }
        $createdAt = Carbon\Carbon::now()->subDays(30);
        $updatedAt = $faker->dateTimeBetween($createdAt);
        // fixed users for testing
        DB::table('users')->insert(
        [
            'full_name' => $faker->name,
            'mec_number' => $faker->numberBetween(0, 9999),
            'type_id' => 2,
            'corporation_id' => 1,
            'role_id' => 1,
            'email' => 'nacional@teste.com',
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
        ]);
        DB::table('users')->insert(
        [
            'full_name' => $faker->name,
            'mec_number' => $faker->numberBetween(0, 9999),
            'type_id' => 2,
            'corporation_id' => 1,
            'role_id' => 2,
            'email' => 'local@teste.com',
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
        ]);
        DB::table('users')->insert(
        [
            'full_name' => $faker->name,
            'mec_number' => $faker->numberBetween(0, 9999),
            'type_id' => 2,
            'corporation_id' => 1,
            'role_id' => 3,
            'email' => 'operador@teste.com',
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
        ]);


    }

        private function fakeUser(Faker\Generator $faker, $typeId, $corporationId, $roleId)
    {
        $createdAt = Carbon\Carbon::now()->subDays(30);
        $updatedAt = $faker->dateTimeBetween($createdAt);
        return [
            'full_name' => $faker->name,
            'mec_number' => $faker->numberBetween(0, 9999),
            'type_id' => $typeId,
            'corporation_id' => $corporationId,
            'role_id' => $roleId,
            'email' => $faker->unique()->safeEmail,
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
        ];
    }
}
