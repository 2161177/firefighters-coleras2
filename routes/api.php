<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
Route::group(['middleware' => ['json.response']], function () {

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return new App\Http\Resources\User($request->user());
    });

    // public routes
    Route::post('/login', 'Auth\LoginController@login');

    // private routes
    Route::middleware('auth:api')->group(function () {
        Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

        // Users
        Route::get('users', 'UserControllerAPI@index');
        Route::get('users/{id}', 'UserControllerAPI@show');
        Route::post('users', 'UserControllerAPI@store');
        Route::put('users/{id}', 'UserControllerAPI@update');
        Route::delete('users/{id}', 'UserControllerAPI@destroy');


        // Cars
        Route::get('cars', 'CarControllerAPI@index');
        Route::get('cars/{id}', 'CarControllerAPI@show');
        Route::post('cars', 'CarControllerAPI@store');
        Route::put('cars/{id}', 'CarControllerAPI@update');


        // Corporations
        Route::get('corporations', 'CorporationControllerAPI@index');
        Route::get('corporations/{id}', 'CorporationControllerAPI@show');
        Route::post('corporations', 'CorporationControllerAPI@store');
        Route::put('corporations/{id}', 'CorporationControllerAPI@update');


        // Operations
        Route::get('operations', 'OperationControllerAPI@index');
        Route::get('operations/{id}', 'OperationControllerAPI@show');
        Route::post('operations', 'OperationControllerAPI@store');
        Route::put('operations/{id}', 'OperationControllerAPI@update');

        Route::get('operations/{operation}/users', 'OperationControllerAPI@getUsers');
        Route::get('operations/{operation}/individual_firefighters', 'OperationControllerAPI@getFirefighters');
        Route::put('operations/{operation}/users', 'OperationControllerAPI@addUser');
        Route::delete('operations/{operation}/users/{user_id}', 'OperationControllerAPI@removeUser');


        // read only //

        // Roles
        Route::get('roles', 'RoleControllerAPI@index');
        Route::get('roles/{id}', 'RoleControllerAPI@show');

        // User_types
        Route::get('user_types', 'UserTypeControllerAPI@index');
        Route::get('user_types/{id}', 'UserTypeControllerAPI@show');

        // Car_classifications
        Route::get('car_classifications', 'CarClassificationControllerAPI@index');
        Route::get('car_classifications/{id}', 'CarClassificationControllerAPI@show');

        // Districts
        Route::get('districts', 'DistrictControllerAPI@index');
        Route::get('districts/{id}', 'DistrictControllerAPI@show');

        // Operation_types
        Route::get('operation_types', 'OperationTypeControllerAPI@index');
        Route::get('operation_types/{id}', 'OperationTypeControllerAPI@show');

        // Operation_severities
        Route::get('operation_severities', 'OperationSeverityControllerAPI@index');
        Route::get('operation_severities/{id}', 'OperationSeverityControllerAPI@show');

        // Combat_phases
        Route::get('combat_phases', 'CombatPhaseControllerAPI@index');
        Route::get('combat_phases/{id}', 'CombatPhaseControllerAPI@show');

        // Statistics
        Route::get('statistics/all', 'StatisticControllerAPI@all');
    });

});


// public api
Route::post('sensor_data', 'SensorDataControllerApi@store');





